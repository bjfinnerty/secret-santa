Secret Santa Application (Practical Interview Test)
===============================

Technologies Used
----------
* Marionette.js, Backbone.js
* Webpack

The Challenge
-------------
* Application that randomly assigns a buyer to a recipient from a defined list in users.json
