module.exports = {
    entry: "./js/entry.js",
    output: {
        publicPath : '/dist',
        path: __dirname + '/dist',
        filename: "bundle.js"
    },

    resolve: {

        root: __dirname,
        modulesDirectories: ['js', 'views', 'node_modules'],

  },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" },
            { test: /\.json$/, exclude: /node_modules/, loader: 'json' },
            { test: /\.handlebars$/, loader: "handlebars-loader" }
        ]
    }
};