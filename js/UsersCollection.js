var _ = require('underscore');

var UserModel = Backbone.Model.extend({

	idAttribute : 'guid',

	defaults:{
		"name": {
			"first": "",
			"last": ""
		},
		"email": "",
		"phone": "",
		"buysFor": null,
		"boughtFor": false
	},

	initialize: function(){

		// The user cannot buy for themselves
		// Store their own id in an array of ids they can't buy for
		// Any additional users that they can't buy for will be added here too
		this.set('cantBuyFor', [this.get('guid')]);

		// Set the display name for convenience
		this.set('displayName', this.get('name').first + ' ' + this.get('name').last)
	},

	pick:function(boughtFor){
		
		// Return the first valid recipient from the shuffled list.
		// NB* this == buyer 
		return _.find(this.collection.shuffle(), function(recipientModel){ 
			if(this.get('cantBuyFor').indexOf(recipientModel.get('guid')) > -1 || recipientModel.get('boughtFor') !== boughtFor){
				return false;
			}else{
				return true;
			}
		}, this)

	},

	gotPicked: function(preventDups, buyersId){

		// Remove this person from subsequent picks by setting the boughtfor flag to true
		// If we aren't allowed buybacks, add the recipients id to the buyers cantBuyFor list
		// NB* this == recipient
		if(preventDups){
			this.get('cantBuyFor').splice(0,0, buyersId)
		}
		this.set('boughtFor', true); 

	}

	
});

var UsersCollection = Backbone.Collection.extend({
	
	model : UserModel,
	url : 'users.json',

	// Parse the users property from the ajax request
	parse: function(resp){
		return resp.users;
	}

});



module.exports = UsersCollection;