var Mn = require('backbone.marionette');
var rootTemplate = require('templates/index.handlebars');
var oneParticipantTemplate = require('templates/OneParticipantTemplate.handlebars');
var _ = require('underscore');
var $ = require('jquery');

var OneParticipantView = Mn.ItemView.extend({

	// Childview of All particiapnts view
	// Renders each row in the table
	template: oneParticipantTemplate,
    tagName: 'tr'

});

var AllParticipantsViewModel = Backbone.Model.extend({

	// model to render and control whether recipients can buy for their buyers (buy backs)
	defaults : {
		preventDuplicates: true
	}
});

module.exports.AllParticipantsView = Mn.CompositeView.extend({

	template : rootTemplate,
	childView : OneParticipantView, 
	childViewContainer: 'tbody',
	tagName: 'table',
	model : new AllParticipantsViewModel(),

	// Make sure that we render the table when the collection is updated from the server
	collectionEvents: {
	    'sync': 'shuffle'
	},

	ui: {
		shuffleButton: '.shuffleButton',
		dupsCheckbox : '.preventDups'
	},

	events: {
		'click @ui.shuffleButton': 'reShuffle',
		'click @ui.dupsCheckbox' : 'changeDups'
	},

	changeDups: function(e){

		// toggle whether buy backs are enabled
		// Set the view-model property to whatever the user has selected and rerender the list
		e.preventDefault();
		var $checkbox = $(e.currentTarget);

		if($checkbox.is(':checked')){
			this.model.set('preventDuplicates', true);
		}else{
			this.model.set('preventDuplicates', false);
		}

		return this.render();
	},


	shuffle: function(){

		var boughtFor = [];

		this.collection.each(function(buyer, idx){

			// Try to pick someone who hasn't been bought for already
			var boughtFor = false;
			var recipient = buyer.pick(boughtFor);

			if(recipient){
				// if we get a valid recipient, link that user to the buyer model
				// Set a flag on the recipient model so that it will be excluded from future picks
				// If we are preventing buybacks, add the buyer to the list of people the recipient can't buy for
				buyer.set('buysFor', recipient.toJSON());
				recipient.gotPicked(this.model.get('preventDuplicates'), buyer.get('guid'));
			}else{
				// Buyer is buying for self so swap with someone else
				// This should only happen on the last model so we can swap with anyone else
				// Choose a valid swapper and set the appropriate flags
				var altRecipient = buyer.pick(!boughtFor);
				buyer.set('buysFor', altRecipient.get('buysFor'));
				altRecipient.set('buysFor', buyer.toJSON());
			}	
		}, this);
		
		// manually trigger a reset to rerender the view
		// return a reference to the view so we can chain methods
		this.collection.trigger('reset');
		return this;

	},


	resetDraw: function(){

		// Loop through the collection and reset any flags that may have been set in a previous draw
		this.collection.each(function(model, index) {
		    model.set({
		    	'boughtFor': false,
		    	'buysFor': null,
		    	'cantBuyFor': [model.get('guid')]
		    }, {silent:true});
		});
		return this;
	},



	reShuffle: function(){
		// Shuffle reassign the pairs and reset the collection
		// Resetting the collection will rerender the list
		return this.resetDraw().shuffle();
	}

});
