var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
var Mn = require('backbone.marionette');
var UsersCollection = require('UsersCollection');
var RootView = require('RootView');



var usersColl = new UsersCollection();

var app = new Mn.Application({
    container : 'body',
    rootView :  new RootView({collection:usersColl})
});

usersColl.fetch().then(function(){
    app.start();
});