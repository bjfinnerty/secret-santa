var Mn = require('backbone.marionette');
var AllParticipantsView = require('UsersViews').AllParticipantsView;

module.exports = Mn.LayoutView.extend({
	
	el: '#app',

	regions : {
		"participantList": "#participant-list"
	},

	initialize: function() {
		// pass the collection down to the composite view
		this.getRegion('participantList').show(new AllParticipantsView({
			collection : this.collection
		}));
	}
});
